Source: tryton-modules-stock-package-shipping-dpd
Section: python
Priority: optional
Maintainer: Debian Tryton Maintainers <team+tryton-team@tracker.debian.org>
Uploaders: Mathias Behrle <mathiasb@m9s.biz>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               jdupes,
               python3-all,
               python3-setuptools,
               python3-sphinx
Standards-Version: 4.7.0
Homepage: https://www.tryton.org/
Vcs-Git: https://salsa.debian.org/tryton-team/tryton-modules-stock-package-shipping-dpd.git
Vcs-Browser: https://salsa.debian.org/tryton-team/tryton-modules-stock-package-shipping-dpd
Rules-Requires-Root: no

Package: tryton-modules-stock-package-shipping-dpd
Architecture: all
Depends: python3-pypdf,
         python3-zeep,
         tryton-modules-party (>= ${version:major}),
         tryton-modules-product (>= ${version:major}),
         tryton-modules-stock (>= ${version:major}),
         tryton-modules-stock-package (>= ${version:major}),
         tryton-modules-stock-package-shipping (>= ${version:major}),
         tryton-modules-stock-shipment-measurements (>= ${version:major}),
         tryton-server (>= ${version:major}),
         ${API},
         ${misc:Depends},
         ${python3:Depends},
         ${sphinxdoc:Depends}
Description: Tryton application platform - stock package shipping dpd module
 Tryton is a high-level general purpose application platform. It is the base
 of a complete business solution as well as a comprehensive health and hospital
 information system (GNUHealth).
 .
 This module allows one to generate the DPD label using DPD webservices.
 .
 Note: There exist different DPD APIs for different countries. This module
 currently supports only the API available at public-ws.dpd.com (mostly used in
 the Netherlands/Belgium). Please assure with your DPD subsidiary that you
 can use this API.
